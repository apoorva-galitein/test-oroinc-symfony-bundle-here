For testing I have created 2 commands which is using bundle: https://bitbucket.org/apoorva-galitein/galitein-symfony-command-chaining/src/master/

[Read this requirement ](https://github.com/mbessolov/test-tasks/blob/master/7.md)

- For demo please run bin/console app:first and bin/console app:second.

- Command app:first is registered in chain of app:second. So it will not run directly. While, app:second will run.
Please see services.yaml for configuration. 

- For more information, please read bundle's readme.

